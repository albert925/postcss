var gulp = require('gulp')
var postcss = require('gulp-postcss')
var cssnext = require('postcss-cssnext')
var autoprefixe = require('autoprefixer')//agregar prefijos plugin postcss
var cssnested = require('postcss-nested')
var mixins = require('postcss-mixins')
var atImport = require('postcss-import')
var csswring = require('csswring') //minificar codigo
var broserSync = require('browser-sync').create()

//servidor de desarrollo
gulp.task('serve', function () {
	broserSync.init({
		server: {
			baseDir: './www'
		}
	})
})

// Tarea para procesar el CSS
gulp.task('css', function () {
	var processors = [
		//plugines
		//autoprefixe({browsers: ['> 5%', 'ie 8']}),
		atImport(),
		mixins(),
		cssnested,
		//cssnext ya tiene autoprefixe
		cssnext({browsers: ['> 5%', 'ie 8']}),
		csswring()
	]
	//return gulp.src('./src/*.css')  //exportar por cada archivo diferente o que se crea
	return gulp.src('./src/style.css')	//En un solo archivo definida de la raiz
		.pipe(postcss(processors))
		.pipe(gulp.dest('./www/css')) //Ruta al exportar
		.pipe(broserSync.stream())
})

//Tarea para vigilar los cambios
gulp.task('watch', function () {
	gulp.watch('./src/*.css', ['css'])
	gulp.watch('./www/*.html').on('change', broserSync.reload)
})

gulp.task('default', ['watch', 'serve'])
